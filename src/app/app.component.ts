import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Service } from './core/service';
import { KeycloakService } from 'keycloak-angular';
import { KeycloakProfile } from 'keycloak-js';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy, OnInit {
  @ViewChild('drawer') drawer: MatSidenav | undefined;
  isLoggedIn = false;
  userProfile: KeycloakProfile | null = null;

  title = 'front-doctor';
  username!: string
  reason = '';
  mobileQuery: MediaQueryList;
  isOnline: boolean

  private _mobileQueryListener: () => void;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    service: Service,
    private keycloak: KeycloakService
  ) {
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery = media.matchMedia('(max-width: 750px)');
    this.mobileQuery.addEventListener('change', this._mobileQueryListener)
    this.isOnline = service.isOnline
    service.connectionState.subscribe(isOnline => this.isOnline = isOnline)
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this._mobileQueryListener);
  }

  public async ngOnInit() {
    this.isLoggedIn = await this.keycloak.isLoggedIn();

    if (this.isLoggedIn) {
      this.userProfile = await this.keycloak.loadUserProfile();
    }
  }

  close(reason: string) {
    if(this.mobileQuery.matches) {
      this.drawer?.close();
    }
  }

  logout() {
    this.keycloak.logout()
  }
}

import { APP_INITIALIZER, NgModule, isDevMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';

import { MaterialModule } from './modules/material.module';
import { DoctorModule } from './modules/doctor/doctor.module';
import { ClinicModule } from './modules/clinic/clinic.module';
import { VisitModule } from './modules/visit/visit.module';
import { MenuComponent } from './shared/components/menu/menu.component';

import { NgxMaskModule, IConfig } from 'ngx-mask'
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { GpsComponent } from './shared/components/gps/gps.component';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { environment } from 'src/environments/environment';
import { AuthGuard } from './guards/auth/auth.guard';

function initializeKeycloak(keycloak: KeycloakService) {
  return () => keycloak.init({
    config: {
      url: environment.url,
      realm: environment.realm,
      clientId: environment.clientId
    },
    initOptions: {
      onLoad: 'check-sso',
      silentCheckSsoRedirectUri: window.location.origin + '/assets/sso/silent-check-sso.html'
    }
  })
}

const maskConfig: Partial<IConfig> = {
  validation: false,
};

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    GpsComponent
  ],
  imports: [
    MaterialModule,
    DoctorModule,
    ClinicModule,
    VisitModule,

    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    KeycloakAngularModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: !isDevMode(),
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
    NgxMaskModule.forRoot(maskConfig)
  ],
  providers: [
    AuthGuard,
    {
      provide: MAT_DATE_LOCALE,
      useValue: 'pt-BR'
    },
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

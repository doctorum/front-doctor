import { FormGroup, FormControl } from "@angular/forms";

export class _Component {
  isEmpty(arr: any[] | undefined) {
    return !arr || arr.length == 0
  }

  _validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this._validateAllFormFields(control);
      }
    });
  }
}

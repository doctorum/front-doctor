import Dexie, { Table } from "dexie";
import DoctorEntity from "./entity/doctor.entity";
import { Injectable } from "@angular/core";

@Injectable({ providedIn: "root" })
export default class DoctorRepository extends Dexie {
    private doctor!: Table<DoctorEntity, number>

    constructor() {
        super('doctordb')
        this.version(3).stores({
            doctor: '++id'
        })
    }

    async findAll(): Promise<DoctorEntity[]> {
        return await this.doctor.toArray()
    }

    async save(doctor: DoctorEntity) {
        console.log(`Saving doctor entity on localDB. ${doctor}`)
        return await this.doctor.add(doctor)
    }

    async remove(doctorId: number) {
        console.log(`Removing doctor ${doctorId} from localDB`)
        return await this.doctor.delete(doctorId)
    }
}
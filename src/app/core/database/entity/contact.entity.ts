export interface Contact {
    id?: number
    number?: number
    whatsapp?: boolean
    personId?: number
}
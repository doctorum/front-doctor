import { Person } from "./person.entity"

export default interface DoctorEntity extends Person {
    crm?: string
    expertise?: string
    graduationYear?: number
    observation?: string
}
export interface Email {
    id?: number
    address?: string
    personId?: number
}
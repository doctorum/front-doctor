import { Contact } from "./contact.entity"
import { Email } from "./email.entity"

export interface Person {
    id?: number
	nickname?: string
	name?: string
	birth?: string
	contact?: Contact
	email?: Email
}
import VisitFilter from "src/app/modules/clinic/pages/home/components/filter-dialog/model/visit-filter";
import { Filter, State } from "src/app/modules/clinic/pages/home/components/filter-dialog/model/filter";
import { FilterClinicRequest } from "../services/models/request/clinic/filter-clinic.request";

export class ClinicMapper {
    private ClinicMapper() {}

    public static toFilterClinicRequest(filters: VisitFilter[]): FilterClinicRequest {
        return filters.reduce<FilterClinicRequest>((prev, filter) => {
            switch (filter.type) {
                case 'Cidade':
                    prev.counties = filter.filters.map(({ id }) => id)
                    break
                case 'Estado':
                    prev.states = filter.filters.map(({ id }) => id)
                    break
                default: prev.expertises = filter.filters.map(({ id }) => id)
            }

            return prev
        }, {})
    }

    public static toFilter(filters: VisitFilter[]): Filter {
        return filters.reduce<Filter>((prev, filter) => {
            switch(filter.type) {
                case 'Cidade':
                    prev.counties = filter.filters
                    break
                case 'Estado':
                    prev.state = filter.filters[0]
                    break
                default: prev.expertises = filter.filters.map(filter => filter.name)
            }
            
            return prev
        }, {})
    }
}
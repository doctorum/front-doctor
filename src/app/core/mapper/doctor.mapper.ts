import { DoctorResponse } from "src/app/modules/doctor/models/response/doctor-response"
import DoctorEntity from "../database/entity/doctor.entity"
import CreateDoctorRequest from "../services/models/request/doctor/create-doctor.request"
import { PageResponse } from "../services/models/response/page.response"

export class DoctorMapper {
    static toDoctorResponseList(doctors: DoctorEntity[]): PageResponse<DoctorResponse> {
        return {content: doctors.map(DoctorMapper.toDoctorResponse)}
    }

    static toDoctorResponse(doctor: DoctorEntity): DoctorResponse {
        return {
            id: doctor.id,
            name: doctor.name,
            nickname: doctor.nickname,
            expertise: doctor.expertise
        }
    }

    static toCreateDoctorRequestList(doctors: DoctorEntity[]): CreateDoctorRequest[] {
        return doctors.map(DoctorMapper.toCreateDoctorRequest)
    }

    static toCreateDoctorRequest(doctor: DoctorEntity): CreateDoctorRequest {
        return {
            birth: doctor.birth,
            contact: doctor.contact?.number,
            crm: doctor.crm,
            email: doctor.email?.address,
            graduationYear: doctor.graduationYear,
            name: doctor.name,
            nickname: doctor.nickname,
            observation: doctor.observation,
            expertise: {
                name: doctor.expertise
            }
        }
    }

    static toDoctorEntity(doctor: CreateDoctorRequest): DoctorEntity {
        return {
            crm: doctor.crm,
            name: doctor.name,
            birth: doctor.birth,
            nickname: doctor.nickname,
            expertise: doctor.expertise?.name,
            graduationYear: doctor.graduationYear,
            email: {
                address: doctor.email
            },
            contact: {
                number: doctor.contact
            }
        }
    }
}
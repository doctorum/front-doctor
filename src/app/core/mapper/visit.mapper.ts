import RecordedAudioResponse from "src/app/shared/components/record/model/response/recorded-audio.response";
import CreateVisitObservationRequest from "../services/models/request/visit/create-visit-observation.request";

export class VisitMapper {
    static toCreateVisitObservation(visitId: number, observation?: string, record?: RecordedAudioResponse): CreateVisitObservationRequest {
        return {
            visitId,
            observation,
            record
        }
    }
}
import { HttpClient } from "@angular/common/http";
import { Injectable, Injector } from "@angular/core";
import { Subject, Subscriber } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
})
export class Service {
    protected static URL = environment.apiServer;
    private connection: Subject<boolean>
    protected http: HttpClient

    constructor(private injector: Injector) {
        this.http = this.injector.get(HttpClient)
        this.connection = new Subject<boolean>()
        window.addEventListener('online', () => this.updateConnection())
        window.addEventListener('offline', () => this.updateConnection())
    }

    get isOnline() {
        return !!window.navigator.onLine
    }

    get connectionState() {
        return this.connection.asObservable()
    }

    private updateConnection() {
        this.connection.next(this.isOnline)
    }
}
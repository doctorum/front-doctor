import { Injectable, Injector } from '@angular/core';
import { Service } from '../service';
import { Observable, map, startWith } from 'rxjs';
import { State } from './models/clinic/state';
import { County } from './models/clinic/county';

@Injectable({
  providedIn: 'root'
})
export class AddressService extends Service {

  constructor(injector: Injector) {
    super(injector)
  }

  listStates(): Observable<State[]> {
    return this.http.get<State[]>(`${Service.URL}/addresses/states`);
  }

  listCountiesByStateId(stateId: number): Observable<County[]> {
    return this.http.get<County[]>(`${Service.URL}/addresses/counties/state/${stateId}`)
  }

  autoCompleteCounties(counties: County[], observable: Observable<any> | undefined) {
    return observable?.pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : (value ? value.name : '')),
        map(value => counties.filter(county => county.name.toLowerCase().includes(value.toLowerCase())))
      )
  }
}

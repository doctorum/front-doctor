import { Injectable, Injector } from '@angular/core';
import { Service } from '../service';
import { Observable, filter } from 'rxjs';
import { ClinicResponse } from './models/response/clinic/clinic-response';
import { CreateClinicRequest } from './models/request/clinic/create-clinic-request';
import { CreateClinicResponse } from './models/response/clinic/create-clinic-response';
import { FilterClinicRequest } from './models/request/clinic/filter-clinic.request';
import { ClinicDetailResponse } from './models/response/clinic/clinic-detail.response';
import { PageRequest } from './models/request/page.request';
import VisitFilter from 'src/app/modules/clinic/pages/home/components/filter-dialog/model/visit-filter';
import { ClinicMapper } from '../mapper/clinic.mapper';
import { PageResponse } from './models/response/page.response';
import { ClinicByIdResponse } from './models/response/clinic/clinic-by-id.response';
import { UpdateClinicRequest } from './models/request/clinic/update-clinic.request';

@Injectable({
  providedIn: 'root'
})
export class ClinicService extends Service {
  constructor(injector: Injector) {
    super(injector)
  }

  listClinics(filters?: VisitFilter[], page?: PageRequest): Observable<PageResponse<ClinicResponse>> {
    const request = ClinicMapper.toFilterClinicRequest(filters || [])
    return this.filterClinics(request, page)
  }

  filterClinics(filters?: FilterClinicRequest, page: PageRequest = { page: 0, size: 10 }): Observable<PageResponse<ClinicResponse>> {
    return this.http.get<PageResponse<ClinicResponse>>(`${Service.URL}/clinics`, {
      params: { ...this.getFieldsWithValue(filters), ...this.getFieldsWithValue(page) }
    })
  }

  private getFieldsWithValue(obj?: any): any {
    return obj
      ? Object.fromEntries(Object.entries(obj).filter(([_, value]) => value !== undefined))
      : {}
  }

  findClinicById(clinicId: number): Observable<ClinicByIdResponse> {
    return this.http.get<ClinicByIdResponse>(`${Service.URL}/clinics/${clinicId}`)
  }

  findDetailsClinic(clinicId: number): Observable<ClinicDetailResponse> {
    return this.http.get<ClinicDetailResponse>(`${Service.URL}/clinics/detail/${clinicId}`)
  }

  createClinic(clinicRequest: CreateClinicRequest): Observable<CreateClinicResponse> {
    return this.http.post<CreateClinicResponse>(`${Service.URL}/clinics`, clinicRequest)
  }

  updateClinic(updateClinicRequest: UpdateClinicRequest): Observable<void> {
    return this.http.put<void>(`${Service.URL}/clinics`, updateClinicRequest)
  }
}

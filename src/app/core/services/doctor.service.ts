import { Observable, catchError, mergeMap, map, of, startWith, forkJoin, from, mergeAll, merge, mapTo } from 'rxjs';
import { Service } from 'src/app/core/service';
import { Injectable, Injector } from '@angular/core';
import { DoctorResponse } from '../../modules/doctor/models/response/doctor-response';
import ExpertiseResponse from './models/response/doctor/expertise.response';
import CreateDoctorRequest from './models/request/doctor/create-doctor.request';
import CreateDoctorResponse from './models/response/doctor/create-doctor.response';
import DoctorRepository from '../database/doctor.repository';
import { DoctorMapper } from '../mapper/doctor.mapper';
import { PageResponse } from './models/response/page.response';
import { PageRequest } from './models/request/page.request';
import { CountyDoctorResponse } from './models/response/doctor/county-doctor.response';
import { FilterDoctorRequest } from './models/request/doctor/filter-doctor.request';
import { DoctorDetailResponse } from './models/response/doctor/doctor-detail.response';
import UpdateDoctorRequest from './models/request/doctor/update-doctor.request';

@Injectable({
  providedIn: 'root'
})
export class DoctorService extends Service {

  constructor(
    injector: Injector,
    private repository: DoctorRepository
  ) {
    super(injector)
    this.syncDoctors()
  }

  listDoctors(filter?: FilterDoctorRequest, page?: PageRequest): Observable<PageResponse<DoctorResponse>> {
    return this.http.get<PageResponse<DoctorResponse>>(`${Service.URL}/doctors`, {
      params: { ...this.getFieldsWithValue(filter), ...this.getFieldsWithValue(page) }
    })
      .pipe(catchError(error => {
        console.warn('Loading from local DB', error)

        return this.repository.findAll()
          .then(DoctorMapper.toDoctorResponseList);
      }))
  }

  private getFieldsWithValue(obj?: any): any {
    return Object.fromEntries(Object.entries(obj)
      .filter(([_, value]) => value))
  }

  listDoctorsByClinic(clinicId: number): Observable<DoctorResponse[]> {
    return this.http.get<DoctorResponse[]>(`${Service.URL}/doctors/clinic/${clinicId}`)
  }

  listCountiesDoctors(): Observable<CountyDoctorResponse[]> {
    return this.http.get<CountyDoctorResponse[]>(`${Service.URL}/doctors/counties`)
  }

  listExpertises(): Observable<ExpertiseResponse[]> {
    return this.http.get<ExpertiseResponse[]>(`${Service.URL}/doctors/expertises`)
  }

  listSectors(): Observable<string[]> {
    return this.http.get<string[]>(`${Service.URL}/doctors/sectors`)
  }

  detailDoctor(doctorId: number): Observable<DoctorDetailResponse> {
    return this.http.get<DoctorDetailResponse>(`${Service.URL}/doctors/detail/${doctorId}`)
  }

  autoCompleteExpertise(expertise: ExpertiseResponse[], observable: Observable<any> | undefined) {
    return observable?.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : (value ? value.name : '')),
      map(value => expertise.filter(expertise => expertise.name.toLowerCase().includes(value.toLowerCase())))
    )
  }

  createDoctor(doctorRequest: CreateDoctorRequest): Observable<CreateDoctorResponse> {
    return this.isOnline
      ? this.createDoctorApi(doctorRequest)
      : this.createOffline(doctorRequest)
  }

  private createDoctorApi(doctorRequest: CreateDoctorRequest): Observable<CreateDoctorResponse> {
    return this.http.post<CreateDoctorResponse>(`${Service.URL}/doctors`, doctorRequest)
      .pipe(catchError(error => {
        console.warn('Saving on localDB', error)

        return this.createOffline(doctorRequest)
      }))
  }

  private createOffline(doctorRequest: CreateDoctorRequest): Observable<CreateDoctorResponse> {
    return new Observable<CreateDoctorResponse>(observable => {
      this.repository.save(DoctorMapper.toDoctorEntity(doctorRequest))
        .then(id => observable.next({ id }))
    })
  }

  updateDoctor(doctorRequest: UpdateDoctorRequest): Observable<void> {
    return this.isOnline
      ? this.updateDoctorApi(doctorRequest)
      : this.updateOffline(doctorRequest)
  }

  private updateDoctorApi(doctorRequest: UpdateDoctorRequest): Observable<void> {
    return this.http.put<void>(`${Service.URL}/doctors`, doctorRequest)
  }

  private updateOffline(doctorRequest: UpdateDoctorRequest): Observable<void> {
    console.warn("Implementing offline update")
    return new Observable(observable => observable.error('Not implemented yet'))
  }

  private syncDoctors() {
    this.connectionState.subscribe(isOnline => {
     
      if (isOnline) {
        console.log('Estou online novamente')
        const doctors = from(this.repository.findAll()).pipe(mergeAll())

        forkJoin([
          doctors.pipe(
            map(DoctorMapper.toCreateDoctorRequest),
            mergeMap(request => this.createDoctorApi(request))
          ),

          doctors.pipe(
            map(entity => entity?.id),
            map(doctorId => doctorId ? this.repository.remove(doctorId) : console.warn(`Doctor with id undefined: {id: ${doctorId}}`))
          )
        ]).subscribe(() => window.location.reload())
      }
    })
  }
}

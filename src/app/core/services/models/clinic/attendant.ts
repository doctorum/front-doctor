import { Contact } from "./contact";
import { Email } from "./email";

export interface Attendant {
    id?: Number,
    nickname?: String,
    contacts?: Contact[],
    emails?: Email[]
}
export interface Contact {
    id?: Number,
    isWhatsapp?: Boolean,
    number: Number
}
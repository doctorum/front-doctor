import { State } from "./state";

export interface County {
    id: number,
    name: string,
    state: State
}
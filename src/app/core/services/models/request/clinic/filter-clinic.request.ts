export interface FilterClinicRequest {
    states?: (string | number)[]
    counties?: (string | number)[]
    expertises?: (string | number)[]
}
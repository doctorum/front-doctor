export interface UpdateClinicRequest {
    id: number
    district: string
    location: string
    publicPlace: string
    email: string
    phone: number
    zipcode: number
    county: County
    latitude: number
    longitude: number
}

interface County {
    id: number
    name: string
    state: State
}

interface State {
    id: number
    name: string
    uf: string
}
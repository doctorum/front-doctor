export default interface CreateDoctorRequest {
    name?: string
    nickname?: string
    birth?: string
    contact?: number
    email?: string
    crm?: string
    expertise?: Expertise
    graduationYear?: number
    observation?: string
    clinics?: number[]
}

interface Expertise {
    name?: string
}
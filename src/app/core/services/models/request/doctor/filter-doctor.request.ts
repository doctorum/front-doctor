export interface FilterDoctorRequest {
    expertises?: string[]
    sectors?: string[]
    counties?: string[]
}
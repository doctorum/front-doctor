export default interface UpdateDoctorRequest {
    id: number
    name?: string
    nickname?: string
    birth?: string
    contact?: number
    email?: string
    crm?: string
    expertise?: Expertise
    graduationYear?: number
    observation?: string
    clinics?: number[]
}

interface Expertise {
    name?: string
}
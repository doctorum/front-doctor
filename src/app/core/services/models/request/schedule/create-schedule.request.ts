export default interface CreateScheduleRequest {
    date: Date
    doctors: number[]
}
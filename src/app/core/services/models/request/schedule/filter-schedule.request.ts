export default interface FilterScheduleRequest {
    names?: string[]
    expertises?: string[]
    counties?: string []
}
export default interface CreateVisitObservationRequest {
    visitId: number
    observation?: string
    record?: RecordRequest
}

interface RecordRequest {
    title: string
    blob: Blob
}
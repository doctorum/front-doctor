export default interface UpdateCheckVisitedDoctorRequest {
    id: number
    visited: boolean
}
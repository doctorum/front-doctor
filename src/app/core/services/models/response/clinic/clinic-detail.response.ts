export interface ClinicDetailResponse {
    id?: number
    name?: string
    publicPlace?: string
    district?: string
    county?: string
    uf?: string
    contact?: number
}
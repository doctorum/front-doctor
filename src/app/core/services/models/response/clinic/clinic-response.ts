export interface ClinicResponse {
    id: number
    district: string
    location: string
    county: County
    contact: number
}

interface County {
    name: string
    state: string
}

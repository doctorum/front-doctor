export interface CountyDoctorResponse {
    id: number
    name: string
}
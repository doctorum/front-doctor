export interface DoctorDetailResponse {
    id?: number
    name?: string
    nickname?: string
    expertise?: string
    birth?: string
    crm?: string
    sector?: string
    graduationYear?: number
    observation?: string
    contact?: ContactResponse
    clinics: ClinicResponse[]
}

interface ContactResponse {
    email: string
    number: number
}

interface ClinicResponse {
    id: number
    name: string
    uf: string
    county: string
}
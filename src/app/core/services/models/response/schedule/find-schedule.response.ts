export default interface FindScheduleResponse {
    id: number
    date: Date
    doctors: DoctorResponse[]
}

export interface DoctorResponse {
    id: number
    nickname: string
    name: string
    county: string
    district: string
    expertise: string
    visited: boolean
}
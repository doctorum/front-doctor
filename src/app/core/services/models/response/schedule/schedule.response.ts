export interface ScheduleResponse {
    id?: number
    visits: VisitResponse[]
}

export interface VisitResponse {
    id: number
    nickname: string
    name: string
    county: string
    district: string
    expertise: string
    observation: string
    audioObs: string
    visited: boolean
    latitude?: number
    longitude?: number
}
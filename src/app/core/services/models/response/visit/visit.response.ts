export default interface VisitResponse {
    id: number
    county?: string
    district?: string
    expertise?: string
    name?: string
    nickname?: string
    observation?: string
    recordName?: string
    audioObservation?: string
    date?: string
}
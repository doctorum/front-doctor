import { Injectable, Injector } from '@angular/core';
import { Service } from '../service';
import { Observable } from 'rxjs';
import { ScheduleResponse } from './models/response/schedule/schedule.response';
import FilterScheduleRequest from './models/request/schedule/filter-schedule.request';
import DateUtil from 'src/app/shared/utils/date-format';
import CreateScheduleRequest from './models/request/schedule/create-schedule.request';
import CreateScheduleResponse from './models/response/schedule/create-schedule.response';
import FindScheduleResponse from './models/response/schedule/find-schedule.response';
import UpdateScheduleRequest from './models/request/schedule/update-schedule.request';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService extends Service {

  constructor(
    injector: Injector
  ) {
    super(injector)
  }

  findById(id: number): Observable<FindScheduleResponse> {
    return this.http.get<FindScheduleResponse>(`${Service.URL}/schedule/${id}`)
  }

  findSchedules(date: Date, filter?: FilterScheduleRequest): Observable<ScheduleResponse> {
    return this.http.get<ScheduleResponse>(`${Service.URL}/schedule`, { params: { date: DateUtil.formateDate(date), ...filter } })
  }

  findSchedulesDays(month: number, year: number): Observable<number[]> {
    return this.http.get<number[]>(`${Service.URL}/schedule/days`, { params: { month, year } })
  }

  updateSchedule(updateRequest: UpdateScheduleRequest): Observable<void> {
    return this.http.patch<void>(`${Service.URL}/schedule`, updateRequest)
  }

  createSchedule(createRequest: CreateScheduleRequest): Observable<CreateScheduleResponse> {
    return this.http.post<CreateScheduleResponse>(`${Service.URL}/schedule`, createRequest)
  }
}

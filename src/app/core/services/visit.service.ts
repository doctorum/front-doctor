import { Injectable, Injector } from '@angular/core';
import { Service } from '../service';
import { Observable } from 'rxjs';
import VisitResponse from './models/response/visit/visit.response';
import CreateVisitObservationResponse from './models/response/visit/create-visit-observation.response';
import CreateVisitObservationRequest from './models/request/visit/create-visit-observation.request';
import UpdateCheckVisitedDoctorRequest from './models/request/visit/update-check-visited-doctor.request';

@Injectable({
  providedIn: 'root'
})
export class VisitService extends Service {

  constructor(injector: Injector) {
    super(injector)
  }

  findById(id: number): Observable<VisitResponse> {
    return this.http.get<VisitResponse>(`${Service.URL}/visit/${id}`)
  }

  save(request: CreateVisitObservationRequest): Observable<CreateVisitObservationResponse> {
    let data = undefined
    if(request.record?.blob) {
      data = new FormData()
      data.append('record', request.record?.blob, request.record.title)
      return this.http.post<CreateVisitObservationResponse>(`${Service.URL}/visit/file`, data, {
        params: {
          visitId: request.visitId,
          observation: request.observation || ''
        }
      })
    } else {
      return this.http.post<CreateVisitObservationResponse>(`${Service.URL}/visit`, request)
    }
  }

  checkVisitedDoctor(updateRequest: UpdateCheckVisitedDoctorRequest): Observable<void> {
    return this.http.put<void>(`${Service.URL}/visit/check`, updateRequest)
  }
}

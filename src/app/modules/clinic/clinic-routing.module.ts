import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./pages/home/home.component";
import { RegisterComponent } from "./pages/register/register.component";
import { ClinicDetailComponent } from "./pages/detail/clinic-detail/clinic-detail.component";
import { AuthGuard } from "src/app/guards/auth/auth.guard";

const routes: Routes = [
    { path: 'clinicas', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'clinicas/cadastro', component: RegisterComponent, canActivate: [AuthGuard] },
    { path: 'clinicas/cadastro/:id', component: RegisterComponent, canActivate: [AuthGuard] },
    { path: 'clinicas/detalhe/:id', component: ClinicDetailComponent, canActivate: [AuthGuard] },
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class ClinicRoutingModule {}
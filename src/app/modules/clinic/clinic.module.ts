import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/modules/material.module';
import { ReactiveFormsModule } from '@angular/forms';

import { HomeComponent } from './pages/home/home.component';
import { ClinicRoutingModule } from './clinic-routing.module';
import { RegisterComponent } from './pages/register/register.component';
import { AttendantsTableComponent } from './pages/register/components/attendants-table/attendants-table.component';
import { AttendantRegisterDialogComponent } from './pages/register/components/attendant-register-dialog/attendant-register-dialog.component';
import { ClinicDetailComponent } from './pages/detail/clinic-detail/clinic-detail.component';
import { FilterComponent } from 'src/app/shared/components/filter/filter.component';
import { FilterDialogComponent } from './pages/home/components/filter-dialog/filter-dialog.component';

@NgModule({
  declarations: [
    HomeComponent,
    RegisterComponent,
    AttendantsTableComponent,
    AttendantRegisterDialogComponent,
    ClinicDetailComponent,
    FilterComponent,
    FilterDialogComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    ClinicRoutingModule
  ]
})
export class ClinicModule { }

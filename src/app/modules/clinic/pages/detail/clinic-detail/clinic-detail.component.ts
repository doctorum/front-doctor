import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { ClinicService } from 'src/app/core/services/clinic.service';
import { DoctorService } from 'src/app/core/services/doctor.service';
import { ClinicDetailResponse } from 'src/app/core/services/models/response/clinic/clinic-detail.response';
import { DoctorResponse } from 'src/app/modules/doctor/models/response/doctor-response';
import { ExpertiseIcon } from 'src/app/shared/enums/expertise-icon';

@Component({
  selector: 'app-clinic-detail',
  templateUrl: './clinic-detail.component.html',
  styleUrls: ['./clinic-detail.component.scss']
})
export class ClinicDetailComponent implements OnInit {
  displayedColumns: string[] = ['expertise', 'nickname', 'name']
  doctors: DoctorResponse[] = []
  clinic: ClinicDetailResponse | undefined

  constructor(
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private doctorService: DoctorService,
    private clinicService: ClinicService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.iconRegistry.addSvgIcon('whatsapp', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/whatsapp.svg'));
  }

  ngOnInit(): void {
    const clinicId: number = this.route.snapshot.params['id']
    this.clinicService.findDetailsClinic(clinicId)
      .subscribe(clinic => this.clinic = clinic)

    this.doctorService.listDoctorsByClinic(clinicId)
      .subscribe(doctors => this.doctors = doctors)
  }

  getIconFromExpertise(expertise: string) {
    return ExpertiseIcon.getIcon(expertise)
  }

  editClinic(clinicId: number | undefined) {
    this.router.navigate([`clinicas/cadastro/${clinicId}`])
  }


  isValidWhtasappNumber(phone: number | undefined) {
    return !!phone && phone.toString().length == 11
  }

  openWhatsapp(phone: number | undefined) {
    /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
      ? window.open(`https://api.whatsapp.com/send?phone=55${phone}`)
      : window.open(`https://web.whatsapp.com/send?phone=55${phone}`)
  }

  back() {
    this.router.navigateByUrl('clinicas')
  }
}

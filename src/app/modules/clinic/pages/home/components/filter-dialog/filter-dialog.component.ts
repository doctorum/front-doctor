import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ClinicMapper } from 'src/app/core/mapper/clinic.mapper';
import { AddressService } from 'src/app/core/services/address.service';
import { DoctorService } from 'src/app/core/services/doctor.service';
import { CountyDoctorResponse } from 'src/app/core/services/models/response/doctor/county-doctor.response';
import ExpertiseResponse from 'src/app/core/services/models/response/doctor/expertise.response';
import { Filter, State } from './model/filter';
import VisitFilter from './model/visit-filter';


@Component({
  selector: 'app-filter-dialog',
  templateUrl: './filter-dialog.component.html',
  styleUrls: ['./filter-dialog.component.scss']
})
export class FilterDialogComponent implements OnInit {
  states?: State[]
  expertises?: ExpertiseResponse[]
  counties?: CountyDoctorResponse[]

  filter: Filter = {}

  constructor(
    @Inject(MAT_DIALOG_DATA) filter: VisitFilter[],
    private dialogRef: MatDialogRef<FilterDialogComponent>,
    
    private doctorService: DoctorService,
    private addressService: AddressService
  ) {
    this.filter = ClinicMapper.toFilter(filter)
  }

  ngOnInit(): void {
    this.doctorService.listExpertises()
      .subscribe(expertises => this.expertises = expertises)
    
    this.addressService.listStates()
      .subscribe(states => this.states = states)
    
    if(this.filter.state) {
      this.findCounties(this.filter.state.id as number)
    }
  }

  apply() {
    this.dialogRef.close([
      { type: 'Estado', filters: this.filter.state ? [this.filter.state] : undefined, color: 'pink' },
      { type: 'Cidade', filters: this.filter.counties, color: 'cyan' },
      { type: 'Especialidade', filters: this.filter.expertises ? (this.filter.expertises).map(name => ({ id: name, name })) : undefined, color: 'green' }
    ].filter(({ filters }) => filters))
  }

  findCounties(stateId: number) {
    this.addressService.listCountiesByStateId(stateId)
      .subscribe(counties => {
        this.counties = counties
      })
  }

  isEmpty(value?: any[]) {
    return !value || value.length === 0
  }

  compareId(option: { id: number }, value: { id: number }) {
    return option.id === value.id
  }
}

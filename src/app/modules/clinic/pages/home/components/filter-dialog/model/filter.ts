export interface Filter {
    expertises?: string[]
    counties?: { id: string | number, name: string}[]
    state?: State
}

export interface State {
    id: string | number,
    name: string,
    uf?: string
}
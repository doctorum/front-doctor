export default interface VisitFilter {
    type: string
    color: string
    filters: {
        id: string | number,
        name: string
    }[]
}
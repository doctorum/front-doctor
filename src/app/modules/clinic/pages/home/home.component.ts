import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AddressService } from 'src/app/core/services/address.service';
import { ClinicService } from 'src/app/core/services/clinic.service';
import { DoctorService } from 'src/app/core/services/doctor.service';
import { State } from 'src/app/core/services/models/clinic/state';
import { ClinicResponse } from 'src/app/core/services/models/response/clinic/clinic-response';
import ExpertiseResponse from 'src/app/core/services/models/response/doctor/expertise.response';
import { FilterDialogComponent } from './components/filter-dialog/filter-dialog.component';
import { FilterClinicRequest } from 'src/app/core/services/models/request/clinic/filter-clinic.request';
import { ClinicMapper } from 'src/app/core/mapper/clinic.mapper';
import { PageRequest } from 'src/app/core/services/models/request/page.request';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  mobileQuery: MediaQueryList
  length!: number;
  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions = [10, 25, 50];

  displayedColumns: String[] = ["location", "county", "state", "district", "options"] 
  clinics: ClinicResponse[] = []
  counties: any[] = []
  states!: State[]
  expertises!: ExpertiseResponse[]
  filterRequest: FilterClinicRequest = {}

  filters: any[] = []

  private _mobileQueryListener: () => void

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private dialog: MatDialog,
    private service: ClinicService,
    private addressService: AddressService,
    private doctorService: DoctorService,
    private router: Router
  ) {
    this._mobileQueryListener = () => {
      
      return changeDetectorRef.detectChanges()
    }

    this.mobileQuery = media.matchMedia('(max-width: 750px)')
    this.mobileQuery.addEventListener('change', this._mobileQueryListener)
  }
  
  ngOnInit(): void {
    this.service.listClinics()
      .subscribe(response => {
        this.clinics = response.content || []
        this.length = response.totalElements || 0
      })

    this.addressService.listStates()
      .subscribe(states => this.states = states)

    this.doctorService.listExpertises()
      .subscribe(expertises => this.expertises = expertises)
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this._mobileQueryListener);
  }

  isEmpty(value: any[]) {
    return !value || value.length === 0
  }

  register() {
    this.router.navigateByUrl('clinicas/cadastro')
  }

  openDialogFilters() {
    this.dialog.open(FilterDialogComponent, {
      width: '80%',
      data: this.filters
    })
      .afterClosed()
      .subscribe((filters: { type: string, color: string, filters: { id: string | number, name: string }[] }[]) => {
        this.filters = filters || this.filters
        this.findClinics()
      })
  }

  findClinics(page: PageEvent = { pageIndex: 0, pageSize: 10, length: 10 }) {
    this.service.listClinics(this.filters, {page: page.pageIndex, size: page.pageSize})
      .subscribe(response => {
        this.clinics = response.content || []
        this.length = response.totalElements || 0
        this.pageSize = page.pageSize
        this.pageIndex = page.pageIndex
      })
  }
}

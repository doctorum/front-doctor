import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendantRegisterDialogComponent } from './attendant-register-dialog.component';

describe('AttendantRegisterDialogComponent', () => {
  let component: AttendantRegisterDialogComponent;
  let fixture: ComponentFixture<AttendantRegisterDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AttendantRegisterDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AttendantRegisterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

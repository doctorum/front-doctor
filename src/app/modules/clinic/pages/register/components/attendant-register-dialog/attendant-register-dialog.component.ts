import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { _Component } from 'src/app/core/abstract/component/component.component';
import { Attendant } from 'src/app/core/services/models/clinic/attendant';
import { Contact } from 'src/app/core/services/models/clinic/contact';
import { Email } from 'src/app/core/services/models/clinic/email';

@Component({
  selector: 'app-attendant-register-dialog',
  templateUrl: './attendant-register-dialog.component.html',
  styleUrls: ['./attendant-register-dialog.component.scss']
})
export class AttendantRegisterDialogComponent extends _Component implements OnInit {
  formAttendant: FormGroup;
  contacts: Contact[] | undefined = [];
  emails: Email[] | undefined = [];

  indexEdit: number | undefined

  constructor(
    public dialogRef: MatDialogRef<AttendantRegisterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public attendant: Attendant,
    private fb: FormBuilder) {
    super()
    this.formAttendant = this.fb.group({
      id: [],
      nickname: ['', Validators.required],
      contact: [''],
      email: ['', Validators.email]
    })
  }

  ngOnInit(): void {
    if (this.attendant) {
      this.formAttendant.patchValue(this.attendant)
      this.emails = this.attendant.emails
      this.contacts = this.attendant.contacts
    }
  }

  save() {
    if (this.formAttendant.valid) {
      const { id, nickname } = this.formAttendant.value
      this.dialogRef.close({ id, nickname, contacts: this.contacts, emails: this.emails })
    } else {
      this._validateAllFormFields(this.formAttendant)
    }
  }

  addContact() {
    const contact = this.formAttendant.get('contact');
    if (contact?.value && contact?.valid) {
      const number: Number = contact?.value

      this.indexEdit == undefined
        ? this.contacts?.push({ number })
        : (this.contacts ? this.contacts[this.indexEdit].number = number : undefined)
      contact.setValue('')
      this.indexEdit = undefined
    }
  }

  editContact(indexContact: number) {
    this.formAttendant.get('contact')?.setValue((this.contacts ? this.contacts[indexContact].number : undefined))
    this.indexEdit = indexContact
  }

  phoneMask(phone: Number): String {
    return phone.toString().length > 9 ? '(00) 9 0000-0000' : '9 0000-0000'
  }

  addEmail() {
    const email = this.formAttendant.get('email')
    if (email?.valid && email?.value != '') {
      const address = email?.value

      this.indexEdit == undefined
        ? this.emails?.push({ address })
        : (this.emails ? this.emails[this.indexEdit].address = address : undefined)
      email.setValue('')
      this.indexEdit = undefined
    }
  }

  editEmail(indexEmail: number) {
    this.formAttendant.get('email')?.setValue(this.emails ? this.emails[indexEmail].address : undefined)
    this.indexEdit = indexEmail
  }

  get nickname() { return this.formAttendant.get('nickname') }
  get email() { return this.formAttendant.get('email') }
  get contact() { return this.formAttendant.get('contact') }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendantsTableComponent } from './attendants-table.component';

describe('AttendantsTableComponent', () => {
  let component: AttendantsTableComponent;
  let fixture: ComponentFixture<AttendantsTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AttendantsTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AttendantsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

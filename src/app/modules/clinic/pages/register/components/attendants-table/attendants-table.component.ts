import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { _Component } from 'src/app/core/abstract/component/component.component';
import { Attendant } from 'src/app/core/services/models/clinic/attendant';
import { AttendantRegisterDialogComponent } from '../attendant-register-dialog/attendant-register-dialog.component';

@Component({
  selector: 'attendants-table',
  templateUrl: './attendants-table.component.html',
  styleUrls: ['./attendants-table.component.scss']
})
export class AttendantsTableComponent extends _Component {
  attendantsColumns: string[] = ['nickname', 'name', 'options'];

  @Input() clinicId: Number | undefined;
  @Input() attendants: Attendant[] = [];

  constructor(
    // private attendantService: AttendantService,
    private dialog: MatDialog,
  ) {
    super()
  }

  openDialog(attendant?: Attendant, index?: number) {
    this.dialog.open(AttendantRegisterDialogComponent, {
      width: '720px',
      data: attendant
    })
      .afterClosed().subscribe(attendant => {
        if (attendant) {
          index == undefined
            ? this.attendants.push(attendant)
            : this.attendants[index] = attendant
        }
      })
  }

  phoneMask(phone: Number): string {
    return phone.toString().length > 9 ? '(00) 9 0000-0000' : '9 0000-0000'
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, first, forkJoin, take } from 'rxjs';
import { _Component } from 'src/app/core/abstract/component/component.component';
import { AddressService } from 'src/app/core/services/address.service';
import { ClinicService } from 'src/app/core/services/clinic.service';
import { Attendant } from 'src/app/core/services/models/clinic/attendant';
import { County } from 'src/app/core/services/models/clinic/county';
import { State } from 'src/app/core/services/models/clinic/state';
import { environment } from 'src/environments/environment';

interface Position {
  latitude: number
  longitude: number
  url: SafeResourceUrl
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent extends _Component implements OnInit {
  public formRegister: FormGroup;
  states: State[] = [];
  counties: Observable<County[]> | undefined;
  clinicId?: number
  attendants: Attendant[] = [];
  position?: Position

  constructor(
    private sanitizer: DomSanitizer,
    private fb: FormBuilder,
    private _snackBar: MatSnackBar,
    private addressService: AddressService,
    private clinicService: ClinicService,
    private route: ActivatedRoute,
    private router: Router) {
      super()
      this.formRegister = this.fb.group({
        id: [undefined],
        location: [undefined, Validators.required],
        zipcode: [undefined],
        district: [undefined],
        publicPlace: [undefined],
        state: [undefined, Validators.required],
        county: [{ value: undefined, disabled: true }],
        email: [undefined],
        phone: [undefined],
        latitude: [undefined],
        longitude: [undefined]
      });

      this.loadPosition()
      this.clinicId = this.route.snapshot.params['id']
      if(this.clinicId) {
        this.loadClinic(this.clinicId)
      }
  }

  private loadClinic(clinicId?: number) {
    if (clinicId) {
      this.clinicService.findClinicById(clinicId)
        .subscribe(response => {
          this.formRegister.patchValue({
            id: response.id,
            location: response.name,
            zipcode: response.zipcode,
            district: response.district,
            publicPlace: response.publicPlace,
            state: response.county.state.id,
            county: response.county,
            email: response.email,
            phone: response.phone,
            latitude: response.latitude,
            longitude: response.longitude
          })
          this.formRegister.get('county')?.enable()

          this.selectState(response.county.state.id, response.county)
        })
    }
  }

  ngOnInit(): void {
    forkJoin({
      states: this.addressService.listStates()
      // location: this.clinicService.findById(this.addressId)
    }).subscribe({
      next: ({ states }) => {
        this.states = states
        // if (location) {
        //   location['state'] = location.county.state.id
        //   this.formRegister.patchValue(location)
        //   this.selectState(location['state'], location.county)
        // }
      }
    })
  }

  private loadPosition() {
      const latitude = this.formRegister.get('latitude')?.value
      const longitude = this.formRegister.get('longitude')?.value

      if (!!latitude && !!longitude) {
        this.position = {
          latitude, longitude,
          url: this.sanitizer.bypassSecurityTrustResourceUrl(`https://www.google.com/maps/embed/v1/place?key=${environment.googleKey}&q=${latitude},${longitude}`)
        }
      } else {
       this.loadCurrentPosition()
     }
  }

  selectState(stateId: number, county?: County) {
    let countyControl = this.formRegister.get('county')
    if (stateId != undefined) {
      this.addressService
        .listCountiesByStateId(stateId)
        .subscribe(counties => {
          this.counties = this.addressService.autoCompleteCounties(counties, countyControl?.valueChanges)
          countyControl?.setValidators(Validators.required)
          countyControl?.setValue(county || '')
          countyControl?.enable()
        })
    } else {
      this.formRegister?.get('state')?.setValue(undefined)
      countyControl?.disable()
      countyControl?.setValue('')
    }
  }

  displayFn(county: County): string {
    return county && county.name ? county.name : '';
  }

  definePosition() {
    this.formRegister.patchValue({
      latitude: this.position?.latitude,
      longitude: this.position?.longitude
    })
    this._snackBar.open('Localização definida para o cliente!', undefined, { duration: 5000, verticalPosition: 'bottom' })
  }

  removePosition() {
    this.formRegister.get('latitude')?.reset()
    this.formRegister.get('longitude')?.reset()
    this.loadCurrentPosition()
    this._snackBar.open('Carregando nova localização!', undefined, { duration: 5000, verticalPosition: 'bottom' })
  }

  private loadCurrentPosition() {
    navigator.geolocation.getCurrentPosition(position => {
      const { latitude, longitude } = position.coords
      this.position = {
        latitude, longitude,
        url: this.sanitizer.bypassSecurityTrustResourceUrl(`https://www.google.com/maps/embed/v1/place?key=${environment.googleKey}&q=${latitude},${longitude}`)
      }
      
    });
  }

  cancel() {
    this.router.navigateByUrl('clinicas')
  }

  save() {
    if (this.formRegister.valid) {
      this.clinicId
        ? this.update()
        : this.create()
    } else {
      this._validateAllFormFields(this.formRegister)
    }
  }

  private create() {
    const createClinicRequest = {
      attendants: this.attendants,
      ...this.formRegister.value
    }
    this.clinicService.createClinic(createClinicRequest)
      .subscribe(_ => {
        this.clinicId = undefined
        this.attendants = []
        this.formRegister.reset()
        this.formRegister.get('county')?.disable()
        this._snackBar.open('Clinica salva com sucesso!', undefined, { duration: 5000, verticalPosition: 'top' })
        this.router.navigateByUrl('clinicas')
      })
  }

  private update() {
    const updateClinicRequest = {
      attendants: this.attendants,
      ...this.formRegister.value
    }
    this.clinicService.updateClinic(updateClinicRequest)
      .subscribe(_ => {
        this._snackBar.open('Clinica atualizada com sucesso!', undefined, { duration: 5000, verticalPosition: 'top' })
        this.router.navigateByUrl('clinicas')
      })
  }

  compareId(option: number, value: number) {
    return option === value
  }

  get location() { return this.formRegister.get('location') }
  get state() { return this.formRegister.get('state') }
  get countyF() { return this.formRegister.get('county') }
  get validePosition() {
    return !!this.formRegister.get('latitude')?.value
      && !!this.formRegister.get('longitude')?.value
  }
}

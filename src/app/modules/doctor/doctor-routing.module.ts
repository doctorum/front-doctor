import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./pages/home/home.component";
import { RegisterComponent } from "./pages/register/register.component";
import { DoctorDetailComponent } from "./pages/detail/doctor-detail/doctor-detail.component";
import { AuthGuard } from "src/app/guards/auth/auth.guard";

const routes: Routes = [
    { path: 'medicos', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'medicos/cadastro', component: RegisterComponent, canActivate: [AuthGuard] },
    { path: 'medicos/cadastro/:id', component: RegisterComponent, canActivate: [AuthGuard] },
    { path: 'medicos/detalhe/:id', component: DoctorDetailComponent, canActivate: [AuthGuard] }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class DoctorRoutingModule {}
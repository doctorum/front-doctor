import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from 'src/app/modules/material.module';
import { HomeComponent } from './pages/home/home.component';
import { RegisterComponent } from './pages/register/register.component';

import { DoctorRoutingModule } from './doctor-routing.module';
import { SelectClinicComponent } from './pages/register/components/select-clinic/select-clinic.component';
import { FilterDialogComponent } from './pages/home/components/filter-dialog/filter-dialog.component';
import { DoctorDetailComponent } from './pages/detail/doctor-detail/doctor-detail.component';
import { IConfig, NgxMaskModule } from 'ngx-mask';

const maskConfig: Partial<IConfig> = {
  validation: false,
};

@NgModule({
  declarations: [
    HomeComponent,
    RegisterComponent,
    SelectClinicComponent,
    FilterDialogComponent,
    DoctorDetailComponent
  ],
  imports: [
    MaterialModule,
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    DoctorRoutingModule,

    NgxMaskModule.forRoot()
  ]
})
export class DoctorModule { }

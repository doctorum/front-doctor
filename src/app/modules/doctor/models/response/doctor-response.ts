export interface DoctorResponse {
    visited?: boolean;
    id?: number;
    expertise?: string;
    name?: string;
    nickname?: string;
}
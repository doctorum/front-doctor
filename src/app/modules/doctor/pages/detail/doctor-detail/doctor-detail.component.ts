import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { DoctorService } from 'src/app/core/services/doctor.service';
import { DoctorDetailResponse } from 'src/app/core/services/models/response/doctor/doctor-detail.response';

@Component({
  selector: 'app-doctor-detail',
  templateUrl: './doctor-detail.component.html',
  styleUrls: ['./doctor-detail.component.scss']
})
export class DoctorDetailComponent implements OnInit {
  columns = ['name', 'uf', 'city']
  doctor: DoctorDetailResponse = { clinics: [] }

  constructor(
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private route: ActivatedRoute,
    private doctorService: DoctorService
  ) {
    this.iconRegistry.addSvgIcon('whatsapp', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/whatsapp.svg'));
  }

  ngOnInit(): void {
    const doctorId = this.route.snapshot.params['id']
    this.doctorService.detailDoctor(doctorId)
      .subscribe(doctorDetail => this.doctor = doctorDetail)
  }


  isValidWhtasappNumber(phone: number | undefined) {
    return !!phone && phone.toString().length == 11
  }

  openWhatsapp(phone: number | undefined) {
    /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
      ? window.open(`https://api.whatsapp.com/send?phone=55${phone}`)
      : window.open(`https://web.whatsapp.com/send?phone=55${phone}`)
  }

  openEmail(email: string | undefined) {
    if(email) {
      window.location.href = `mailto:${email}`
    }
  }
}

import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DoctorService } from 'src/app/core/services/doctor.service';
import { CountyDoctorResponse } from 'src/app/core/services/models/response/doctor/county-doctor.response';
import ExpertiseResponse from 'src/app/core/services/models/response/doctor/expertise.response';

interface Filter {
  sectors?: string[]
  expertises?: string[]
  counties?: string[]
}

@Component({
  selector: 'app-filter-dialog',
  templateUrl: './filter-dialog.component.html',
  styleUrls: ['./filter-dialog.component.scss']
})
export class FilterDialogComponent implements OnInit {
  states = [{ id: 1, uf: 'ma' }]
  expertises!: ExpertiseResponse[]
  sectors!: string[]
  counties!: CountyDoctorResponse[]
  filter: Filter = {}

  constructor(
    @Inject(MAT_DIALOG_DATA) filter: Filter,

    private doctorService: DoctorService,
    private dialogRef: MatDialogRef<FilterDialogComponent>
  ) {
    this.filter = filter
  }

  ngOnInit(): void {
    this.doctorService.listExpertises()
      .subscribe(expertises => this.expertises = expertises)
    
    this.doctorService.listSectors()
      .subscribe(sectors => this.sectors = sectors)
    
    this.doctorService.listCountiesDoctors()
      .subscribe(counties => this.counties = counties)
  }

  applyFilter() {
    this.dialogRef.close([
      { type: 'Setor', filters: this.filter.sectors, color: 'cyan' },
      { type: 'Especialidade', filters: this.filter.expertises, color: 'green' },
      { type: 'Cidade', filters: this.filter.counties, color: 'pink' }
    ].filter(({ filters }) => filters))
  }
}

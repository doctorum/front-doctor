import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DoctorService } from 'src/app/core/services/doctor.service';
import { DoctorResponse } from '../../models/response/doctor-response';
import { ExpertiseIcon } from 'src/app/shared/enums/expertise-icon';
import { PageEvent } from '@angular/material/paginator';
import { MediaMatcher } from '@angular/cdk/layout';
import { MatDialog } from '@angular/material/dialog';
import { FilterDialogComponent } from './components/filter-dialog/filter-dialog.component';
import { FilterDoctorRequest } from 'src/app/core/services/models/request/doctor/filter-doctor.request';
import { PageRequest } from 'src/app/core/services/models/request/page.request';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  mobileQuery: MediaQueryList;
  length!: number;
  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions = [10, 25, 50];

  displayedColumns: string[] = ['expertise', 'name', 'options']
  doctors: DoctorResponse[] = []
  filtersList!: { type: string, filters: string[], color: string }[]
  
  private _mobileQueryListener: () => void;

  constructor(
    private doctorService: DoctorService,
    private router: Router,
    public dialog: MatDialog,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher
  ) {
    this._mobileQueryListener = () => {
      this.displayedColumns = this.mobileQuery.matches ? ['expertise', 'name'] : ['expertise', 'name', 'options']
      return changeDetectorRef.detectChanges()
    };

    this.mobileQuery = media.matchMedia('(max-width: 750px)');
    this.mobileQuery.addEventListener('change', this._mobileQueryListener);
    this.displayedColumns = this.mobileQuery.matches ? ['expertise', 'name'] : ['expertise', 'name', 'options']
  }

  ngOnInit(): void {
    this.doctorService.listDoctors({}, { page: this.pageIndex, size: this.pageSize })
      .subscribe(response => {
        this.doctors = response.content || []
        this.length = response.totalElements || 0
      })
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this._mobileQueryListener);
  }

  getIconFromExpertise(expertise: string) {
    return ExpertiseIcon.getIcon(expertise)
  }

  isListEmpty(values: any[]): boolean {
    return values?.length > 0;
  }

  register() {
    this.router.navigateByUrl('medicos/cadastro')
  }

  removeFilter(list: string[], value: string) {
    const index = list.findIndex(filter => filter === value)
    list.splice(index, 1)
    this.filtersList = this.filtersList.filter(filter => filter.filters.length > 0)
    this.filterDoctors()
  }

  openFilter() {
    this.dialog.open(FilterDialogComponent, {
      width: '80%',
      data: {
        sectors: this.getFilters('Setor'),
        expertises: this.getFilters('Especialidade'),
        counties: this.getFilters('Cidade')
      }
    })
    .afterClosed()
    .subscribe(filters => {
      if(filters) {
        this.filtersList = filters
        this.filterDoctors()
      }
    })
  }

  filterDoctors(page: PageRequest = { page: 0, size: 10 }) {
    return this.doctorService.listDoctors({
      sectors: this.getFilters('Setor'),
      expertises: this.getFilters('Especialidade'),
      counties: this.getFilters('Cidade')
    }, page)
      .subscribe(response => {
        this.doctors = response.content || []
        this.length = response.totalElements || 0
        this.pageSize = page.size
        this.pageIndex = page.page
      })
  }

  private getFilters(type: string) {
    return this.filtersList
      ?.find(filter => filter.type === type)
      ?.filters
  }

  findDoctors(page: PageEvent) {
    this.filterDoctors({ page: page.pageIndex, size: page.pageSize })
  }
}

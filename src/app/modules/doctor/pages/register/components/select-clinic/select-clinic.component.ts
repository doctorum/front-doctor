import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AddressService } from 'src/app/core/services/address.service';
import { ClinicService } from 'src/app/core/services/clinic.service';
import { State } from 'src/app/core/services/models/clinic/state';
import { ClinicResponse } from 'src/app/core/services/models/response/clinic/clinic-response';


interface FilterForm {
  stateId?: number
  counties?: number[]
}

@Component({
  selector: 'app-select-clinic',
  templateUrl: './select-clinic.component.html',
  styleUrls: ['./select-clinic.component.scss']
})
export class SelectClinicComponent implements OnInit {
  displayedColumns: String[] = ['check', 'name', 'district', 'city']
  clinics: ClinicResponse[] = []
  districts: {id: number, name: string}[] = []
  counties: any[] = []
  states!: State[]

  clinicsSelected: ClinicResponse[] = []
  filterForm: FilterForm = {}

  constructor(
    private clinicService: ClinicService,
    private addressService: AddressService,
    public dialogRef: MatDialogRef<SelectClinicComponent>
  ) {}
  
  ngOnInit(): void {
    this.addressService.listStates()
      .subscribe(states => this.states = states)
  }

  findCounties(stateId: number) {
    this.addressService.listCountiesByStateId(stateId)
      .subscribe(counties => {
        this.filterForm.stateId = stateId
        this.counties = counties
      })
  }

  selectCounties(counties: number[]) {
    this.filterForm.counties = counties
  }

  filter() {
    this.clinicService.filterClinics({
      states: this.filterForm.stateId ? [this.filterForm.stateId] : [],
      counties: this.filterForm.counties
    })
    .subscribe(response => this.clinics = response.content || []);
  }

  selectClinic(clinic: ClinicResponse, checked: boolean) {
    if (checked) this.clinicsSelected.push(clinic)
    else this.clinicsSelected = this.clinicsSelected.filter(c => c.id != clinic.id)
  }

  close() {
    this.dialogRef.close(this.clinicsSelected)
  }

  isEmpty(value: any[]) {
    return !value || value.length === 0
  }
}

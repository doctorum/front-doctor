import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { _Component } from 'src/app/core/abstract/component/component.component';
import { DoctorService } from 'src/app/core/services/doctor.service';
import ExpertiseResponse from 'src/app/core/services/models/response/doctor/expertise.response';
import { SelectClinicComponent } from './components/select-clinic/select-clinic.component';

interface ClinicDoctorRequest {
  id?: number
  location?: string
  district?: string
  county?: { name: string }
}


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent extends _Component implements OnInit {
  columns = ['name', 'district', 'city']
  clinics: ClinicDoctorRequest[] = []
  expertises: Observable<ExpertiseResponse[]> | undefined
  public formRegister: FormGroup
  private doctorId!: number

  constructor(
    private doctorService: DoctorService,
    private _snackBar: MatSnackBar,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog
  ) {
    super()
    this.formRegister = this.fb.group({
      id: [undefined],
      nickname: [undefined],
      name: [undefined, Validators.required],
      birth: [undefined],
      contact: [undefined],
      email: [undefined],
      crm: [undefined],
      expertise: [undefined],
      graduationYear: [undefined],
      observation: [undefined]
    })

    this.doctorId = this.route.snapshot.params['id']
    if (this.doctorId) {
      this.doctorService.detailDoctor(this.doctorId)
        .subscribe(response => {
          this.formRegister.patchValue({
            id: response.id,
            nickname: response.nickname,
            name: response.name,
            birth: response.birth,
            contact: response.contact?.number,
            email: response.contact?.email,
            crm: response.crm,
            expertise: { name: response.expertise },
            graduationYear: response.graduationYear,
            observation: response.observation
        })

          this.clinics = response.clinics.map<ClinicDoctorRequest>(clinic => ({
          id: clinic.id,
          location: clinic.name,
          county: { name: clinic.county }
        }))
      })
    }
  }

  ngOnInit(): void {
    this.doctorService.listExpertises()
      .subscribe(expertises => {
        let expertiseControl = this.formRegister?.get('expertise')
        this.expertises = this.doctorService.autoCompleteExpertise(expertises, expertiseControl?.valueChanges)
      })
  }

  save() {
    if (this.formRegister.valid) {
      this.isCreateDoctor()
        ? this.create()
        : this.update()
    } else {
      this._validateAllFormFields(this.formRegister)
    }
  }

  private isCreateDoctor(): boolean {
    return !this.doctorId
  }

  private create() {
    this.doctorService.createDoctor({
      clinics: this.clinics.map(({ id }) => id),
      ...this.formRegister.value
    })
      .subscribe(() => {
        this.formRegister.reset()
        this._snackBar.open('Médico cadastrado com sucesso!', undefined, { duration: 5000, verticalPosition: 'top' })
        this.router.navigateByUrl('medicos')
      })
  }

  private update () {
    this.doctorService.updateDoctor({
      clinics: this.clinics.map(({ id }) => id),
      ...this.formRegister.value
    })
      .subscribe(() => {
        this.formRegister.reset()
        this._snackBar.open('Médico atualizado com sucesso!', undefined, { duration: 5000, verticalPosition: 'top' })
        this.router.navigateByUrl('medicos')
      })
  }

  cancel() {
    this.router.navigateByUrl('medicos')
  }

  displayFn(expertise: ExpertiseResponse): string {
    return expertise && expertise.name ? expertise.name : '';
  }

  selectClinicDialog() {
    this.dialog.open(SelectClinicComponent, {
      width: '80%',
    }).afterClosed().subscribe(clinics => {
      if(clinics) {
        this.clinics = clinics
      }
    })
  }

  get name() { return this.formRegister?.get('name') }
}

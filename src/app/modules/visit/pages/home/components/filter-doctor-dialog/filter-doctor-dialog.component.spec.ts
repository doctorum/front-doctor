import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterDoctorDialogComponent } from './filter-doctor-dialog.component';

describe('FilterDoctorDialogComponent', () => {
  let component: FilterDoctorDialogComponent;
  let fixture: ComponentFixture<FilterDoctorDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilterDoctorDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FilterDoctorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

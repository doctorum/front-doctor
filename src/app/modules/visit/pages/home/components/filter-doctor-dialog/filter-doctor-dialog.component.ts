import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DoctorService } from 'src/app/core/services/doctor.service';
import { CountyDoctorResponse } from 'src/app/core/services/models/response/doctor/county-doctor.response';
import ExpertiseResponse from 'src/app/core/services/models/response/doctor/expertise.response';

interface Filter {
  names: string[]
  expertises?: string[]
  counties?: string[]
}

@Component({
  selector: 'app-filter-doctor-dialog',
  templateUrl: './filter-doctor-dialog.component.html',
  styleUrls: ['./filter-doctor-dialog.component.scss']
})
export class FilterDoctorDialogComponent implements OnInit {
  expertises!: ExpertiseResponse[]
  counties!: CountyDoctorResponse[]
  filter: Filter = { names: [] }

  formControl = new FormControl([])

  constructor(
    @Inject(MAT_DIALOG_DATA) filter: Filter,
    private dialogRef: MatDialogRef<FilterDoctorDialogComponent>,
    private doctorService: DoctorService
  ) {
    this.filter = { ...filter }
  }

  ngOnInit(): void {
    this.doctorService.listExpertises()
      .subscribe(expertises => this.expertises = expertises)

    this.doctorService.listCountiesDoctors()
      .subscribe(counties => this.counties = counties)
  }

  removeKeyword(name: string) {
    const index = this.filter.names.indexOf(name);
    if (index >= 0) {
      this.filter.names.splice(index, 1);
    }
  }

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    if (value) {
      this.filter.names.push(value);
    }

    event.chipInput!.clear();
  }

  applyFilter() {
    this.dialogRef.close([
      { type: 'Cidade', filters: this.filter.counties, color: 'cyan' },
      { type: 'Especialidade', filters: this.filter.expertises, color: 'green' },
      { type: 'Nome', filters: this.filter.names, color: 'pink' }
    ].filter(({ filters }) => filters))
  }
}

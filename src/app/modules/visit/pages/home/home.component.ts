import { ChangeDetectorRef, Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatCalendar, MatCalendarCellClassFunction, MatCalendarHeader } from '@angular/material/datepicker';
import { MatDialog } from '@angular/material/dialog';
import { ExpertiseIcon } from 'src/app/shared/enums/expertise-icon';
import { FilterDoctorDialogComponent } from './components/filter-doctor-dialog/filter-doctor-dialog.component';
import { ScheduleService } from 'src/app/core/services/schedule.service';
import { ScheduleResponse, VisitResponse } from 'src/app/core/services/models/response/schedule/schedule.response';
import FilterScheduleRequest from 'src/app/core/services/models/request/schedule/filter-schedule.request';
import { ActivatedRoute, Router } from '@angular/router';
import UpdateCheckVisitedDoctorRequest from 'src/app/core/services/models/request/visit/update-check-visited-doctor.request';
import { VisitService } from 'src/app/core/services/visit.service';
import DateUtil from 'src/app/shared/utils/date-format';
import { MediaMatcher } from '@angular/cdk/layout';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { GpsComponent } from 'src/app/shared/components/gps/gps.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @ViewChild(MatCalendar) calendar!: MatCalendar<Date>
  calendarHeader = CalendarHeaderComponent

  rippleUnbounded = false
  displayedColumns = ['expertise', 'name', 'location', 'options']
  selected: Date | null = new Date()
  filtersList!: { type: string, filters: string[], color: string }[]
  filter!: FilterScheduleRequest
  daysWithScheduling: number[] = []
  schedule: ScheduleResponse = { visits: [] }
  mobileQuery: MediaQueryList;

  private _mobileQueryListener: () => void;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private _bottomSheet: MatBottomSheet,
    private dialog: MatDialog,
    private scheduleService: ScheduleService,
    private visitService: VisitService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this._mobileQueryListener = () => changeDetectorRef.detectChanges()
    this.mobileQuery = media.matchMedia('(max-width: 750px)')
    this.mobileQuery.addEventListener('change', this._mobileQueryListener)
  }

  ngOnInit(): void {
    const { date } = this.route.snapshot.queryParams

    if(date) {
      this.selected = DateUtil.parseDate(date)
    }
    this.selectMonth(this.selected)
    this.loadDate(this.selected)
  }

  dateClass: MatCalendarCellClassFunction<Date> = (cellDate, view) => {
    if (view === 'month') {
      const date = cellDate.getDate();
      return this.daysWithScheduling.includes(date) ? 'has-visit-enabled' : '';
    }

    return '';
  }

  getIconFromExpertise(expertise: string) {
    return ExpertiseIcon.getIcon(expertise)
  }

  check(visit: UpdateCheckVisitedDoctorRequest) {
    // save on db by api
    this.visitService.checkVisitedDoctor({
      id: visit.id,
      visited: !visit.visited
    })
    .subscribe(() => {
      visit.visited = !visit.visited
    })
  }

  openFilter() {
    this.dialog.open(FilterDoctorDialogComponent, {
      width: '80%',
      data: {
        names: this.getFilters('Nome') || [],
        expertises: this.getFilters('Especialidade'),
        counties: this.getFilters('Cidade')
      }
    })
    .afterClosed()
      .subscribe(filters => {
        if(filters) {
          this.filtersList = filters
          this.filter = {
            names: this.getFilters('Nome') || [],
            counties: this.getFilters('Cidade') || [],
            expertises: this.getFilters('Especialidade') || []
          }
          this.loadDate(this.selected, this.filter)
        }
      })
  }

  private getFilters(type: string) {
    return this.filtersList
      ?.find(filter => filter.type === type)
      ?.filters
  }

  loadDate(date: Date | null, filter?: FilterScheduleRequest) {
    if(date) {
      this.scheduleService.findSchedules(date, filter)
        .subscribe({
          next: schedule => this.schedule = schedule,
          error: error => {
            this.schedule = { visits: [] }
            console.error(error)
          }
      })
    }
  }

  selectMonth(date: Date | null) {
    if (date) {
      this.scheduleService.findSchedulesDays((date.getMonth() + 1), date.getFullYear())
        .subscribe(daysWithScheduling => {
          this.daysWithScheduling = daysWithScheduling
          this.calendar?.monthView._init()
        })
    }
  }

  removeFilter(list: string[], value: string) {
    const index = list.findIndex(filter => filter === value)
    list.splice(index, 1)
    this.filtersList = this.filtersList.filter(filter => filter.filters.length > 0)
    this.filter = {
      names: this.getFilters('Nome') || [],
      counties: this.getFilters('Cidade') || [],
      expertises: this.getFilters('Especialidade') || []
    }
    this.loadDate(this.selected, this.filter)
  }

  addSchedule() {
    this.schedule.id
      ? this.router.navigateByUrl(`/visitas/cadastro/${this.schedule.id}`)
      : this.router.navigate(['/visitas/cadastro'], {
        queryParams: { date: this.selected }
      })
  }

  addObservation(visit: ScheduleResponse) {
    this.router.navigate([`/visitas/${visit.id}/observation`])
  }

  isEmpty(values: any[]) {
    return !values || values.length === 0
  }

  hasLocation(visit: VisitResponse) {
    return !!visit.latitude && !!visit.longitude
  }

  openLocation(visit: VisitResponse) {
    this._bottomSheet.open(GpsComponent, {
      panelClass: 'bottom-sheet',
      data: { latitude: visit.latitude, longitude: visit.longitude }
    });
  }
}

@Component({
  selector: 'example-header',
  template: `
    <div class="mat-calendar-header">
    <div class="mat-calendar-controls">
      <button mat-button type="button" class="mat-calendar-period-button"
              (click)="currentPeriodClicked()" [attr.aria-label]="periodButtonLabel"
              cdkAriaLive="polite">
        {{periodButtonText}}
        <div class="mat-calendar-arrow"
            [class.mat-calendar-invert]="calendar.currentView != 'month'"></div>
      </button>

      <div class="mat-calendar-spacer"></div>

      <ng-content></ng-content>

                  <!--see that we change previousClicked by customPrev-->
      <button mat-icon-button type="button" class="mat-calendar-previous-button"
              [disabled]="!previousEnabled()" (click)="customPrev()"
              [attr.aria-label]="prevButtonLabel">
      </button>

                  <!--see that we change nextClicked by customNext-->
      <button mat-icon-button type="button" class="mat-calendar-next-button"
              [disabled]="!nextEnabled()" (click)="customNext()"
              [attr.aria-label]="nextButtonLabel">
      </button>
    </div>
  </div>
  `
})
export class CalendarHeaderComponent extends MatCalendarHeader<any> {
  customPrev(): void {
    this.previousClicked()
    this.calendar.monthSelected.emit(this.calendar.activeDate)
  }

  customNext(): void {
    this.nextClicked()
    this.calendar.monthSelected.emit(this.calendar.activeDate)
  }
}

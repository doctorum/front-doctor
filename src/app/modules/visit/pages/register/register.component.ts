import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { DoctorService } from 'src/app/core/services/doctor.service';
import { PageRequest } from 'src/app/core/services/models/request/page.request';
import { DoctorResponse } from 'src/app/modules/doctor/models/response/doctor-response';
import { ExpertiseIcon } from 'src/app/shared/enums/expertise-icon';
import { FilterDoctorDialogComponent } from '../home/components/filter-doctor-dialog/filter-doctor-dialog.component';
import FilterScheduleRequest from 'src/app/core/services/models/request/schedule/filter-schedule.request';
import { ActivatedRoute, Router } from '@angular/router';
import { ScheduleService } from 'src/app/core/services/schedule.service';
import DateUtil from 'src/app/shared/utils/date-format';

interface Doctor {
  id: number
  visited: boolean
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  length!: number;
  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions = [10, 25, 50];

  filter!: FilterScheduleRequest
  filtersList!: { type: string, filters: string[], color: string }[]
  displayedColumns: string[] = ['expertise', 'name', 'options']
  doctors: DoctorResponse[] = []

  doctorsAdded: Doctor[] = []
  date!: Date
  scheduleId!: number

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    private doctorService: DoctorService,
    private scheduleService: ScheduleService
  ) {
  }

  ngOnInit(): void {
    this.scheduleId = this.route.snapshot.params['id']
    if(this.scheduleId) {
      this.scheduleService
        .findById(this.scheduleId)
        .subscribe(response => {
          this.date = new Date(`${response.date}T00:00:00`)
          this.doctorsAdded = response.doctors
          this.filterDoctors()
        })
    } else {
      this.route.queryParams
        .subscribe(({ date }) => {
          this.date = new Date(date)
          this.filterDoctors()
        })
    }
  }

  removeFilter(list: string[], value: string) {
    const index = list.findIndex(filter => filter === value)
    list.splice(index, 1)
    this.filtersList = this.filtersList.filter(filter => filter.filters.length > 0)
    this.filterDoctors()
  }

  filterDoctors(page: PageRequest = { page: 0, size: 10 }) {
    return this.doctorService.listDoctors({
      sectors: this.getFilters('Setor'),
      expertises: this.getFilters('Especialidade'),
      counties: this.getFilters('Cidade')
    }, page)
      .subscribe(response => {
        this.doctors = (response.content || [])
          .map(doctor => {
            doctor.visited = this.isVisited(doctor.id)
            return doctor
          })
        this.length = response.totalElements || 0
        this.pageSize = page.size
        this.pageIndex = page.page
      })
  }

  private isVisited(id: number = -1): boolean {
    return this.doctorsAdded
      .map(({ id }) => id)
      .includes(id)
  }

  private getFilters(type: string) {
    return this.filtersList
      ?.find(filter => filter.type === type)
      ?.filters
  }

  getIconFromExpertise(expertise: string) {
    return ExpertiseIcon.getIcon(expertise)
  }

  findDoctors(page: PageEvent) {
    this.filterDoctors({ page: page.pageIndex, size: page.pageSize })
  }

  check(visit: Doctor) {
    visit.visited = !visit.visited
    if (visit.visited) {
      this.doctorsAdded.push(visit)
    } else {
      const index = this.doctorsAdded.findIndex(doctor => doctor.id === visit.id)
      this.doctorsAdded.splice(index, 1)
    }
    console.log(this.doctorsAdded)
  }

  openFilter() {
    this.dialog.open(FilterDoctorDialogComponent, {
      width: '80%',
      data: {
        names: this.getFilters('Nome') || [],
        expertises: this.getFilters('Especialidade'),
        counties: this.getFilters('Cidade')
      }
    })
      .afterClosed()
      .subscribe(filters => {
        if (filters) {
          this.filtersList = filters
          this.filter = {
            names: this.getFilters('Nome') || [],
            counties: this.getFilters('Cidade') || [],
            expertises: this.getFilters('Especialidade') || []
          }
          this.filterDoctors()
        }
      })
  }

  save() {
    this.scheduleId
      ? this.update()
      : this.create()
  }

  private create() {
    this.scheduleService.createSchedule({
      date: this.date,
      doctors: this.doctorsAdded.map(doctor => doctor.id)
    })
      .subscribe(() => this.router.navigate(['/visitas'], {
        queryParams: { date: DateUtil.formateDate(this.date) }
      }))
  }

  private update() {
    this.scheduleService.updateSchedule({
      id: this.scheduleId,
      doctors: this.doctorsAdded.map(doctor => doctor.id)
    })
      .subscribe(() => this.router.navigate(['/visitas'], {
        queryParams: { date: DateUtil.formateDate(this.date) }
      }))
  }
}

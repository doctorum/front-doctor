import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitObservationComponent } from './visit-observation.component';

describe('VisitObservationComponent', () => {
  let component: VisitObservationComponent;
  let fixture: ComponentFixture<VisitObservationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisitObservationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VisitObservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

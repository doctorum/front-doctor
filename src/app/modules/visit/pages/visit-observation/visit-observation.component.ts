import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VisitMapper } from 'src/app/core/mapper/visit.mapper';
import VisitResponse from 'src/app/core/services/models/response/visit/visit.response';
import { VisitService } from 'src/app/core/services/visit.service';
import RecordedAudioResponse from 'src/app/shared/components/record/model/response/recorded-audio.response';
import DateUtil from 'src/app/shared/utils/date-format';

@Component({
  selector: 'app-visit-observation',
  templateUrl: './visit-observation.component.html',
  styleUrls: ['./visit-observation.component.scss']
})
export class VisitObservationComponent implements OnInit {
  visit?: VisitResponse
  visitId?: number
  observation?: string
  audio?: RecordedAudioResponse

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: VisitService
  ) {}

  ngOnInit(): void {
    this.visitId = this.route.snapshot.params['id']
    if (this.visitId) {
      this.service.findById(this.visitId)
        .subscribe(visit => {
          this.visit = visit
          this.observation = visit.observation
        })
    }
  }

  save() {
    if(this.visitId) {
      this.service.save(VisitMapper.toCreateVisitObservation(
        this.visitId,
        this.observation,
        this.audio
      ))
        .subscribe(response => this.router.navigate([`/visitas`], {
          queryParams: { date: this.visit?.date }
        }))
    }
  }

  onAudioRecord(record: RecordedAudioResponse) {
    this.audio = record
  }

  onClearRecord() {
    this.audio = undefined
  }

  isSaved() {
    return this.audio
      || this.observation && this.observation.length > 0
  }

  navigateBack() {
    this.router.navigate(['visitas'], {
      queryParams: { date: this.visit?.date }
    })
  }
}

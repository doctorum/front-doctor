import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./pages/home/home.component";
import { RegisterComponent } from "./pages/register/register.component";
import { VisitObservationComponent } from "./pages/visit-observation/visit-observation.component";
import { AuthGuard } from "src/app/guards/auth/auth.guard";

const routes: Routes = [
    { path: '', redirectTo: 'visitas', pathMatch: "full" },
    { path: 'visitas', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'visitas/cadastro', component: RegisterComponent, canActivate: [AuthGuard] },
    { path: 'visitas/cadastro/:id', component: RegisterComponent, canActivate: [AuthGuard] },
    { path: 'visitas/:id/observation', component: VisitObservationComponent, canActivate: [AuthGuard] }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class VisitRoutingModule {}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CalendarHeaderComponent, HomeComponent } from './pages/home/home.component';
import { VisitRoutingModule } from './visit-routing.module';

import { MaterialModule } from 'src/app/modules/material.module';
import { FilterDoctorDialogComponent } from './pages/home/components/filter-doctor-dialog/filter-doctor-dialog.component';
import { RegisterComponent } from './pages/register/register.component';
import { VisitObservationComponent } from './pages/visit-observation/visit-observation.component';
import { RecordComponent } from 'src/app/shared/components/record/record.component';

@NgModule({
  declarations: [
    HomeComponent,
    FilterDoctorDialogComponent,
    CalendarHeaderComponent,
    RegisterComponent,
    VisitObservationComponent,
    RecordComponent
  ],
  imports: [
    CommonModule,
    FormsModule,

    VisitRoutingModule,
    ReactiveFormsModule,
    MaterialModule
  ]
})
export class VisitModule { }

import { Component, EventEmitter, Input, Output } from '@angular/core';

interface Filter {
  id: number | string
  name: string
}

interface ItemFilter {
  type: string,
  color: string,
  filters: Filter[]
}

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent {
  @Output() onRemoveFilter = new EventEmitter<Filter>()
  @Input() filtersList?: ItemFilter[]

  removeFilter(list: Filter[], value: Filter) {
    const index = list.findIndex(filter => filter.id === value.id)
    list.splice(index, 1)
    this.filtersList = this.filtersList?.filter(filter => filter.filters.length > 0)
    this.onRemoveFilter.emit(value)
  }
}

import { Component, Inject } from '@angular/core';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

interface Position {
  latitude: number
  longitude: number
}

@Component({
  selector: 'app-gps',
  templateUrl: './gps.component.html',
  styleUrls: ['./gps.component.scss']
})
export class GpsComponent {
  position: Position

  constructor(
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private _bottomSheetRef: MatBottomSheetRef<GpsComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) position: Position,
  ) {
    this.iconRegistry.addSvgIcon('gmaps', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/gmaps.svg'));
    this.iconRegistry.addSvgIcon('waze', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/waze.svg'));
    this.position = position
  }

  openWaze() {
    window.open(`https://waze.com/ul?ll=${this.position.latitude}%2C${this.position.longitude}&amp;navigate=yes`, '_self')
    this._bottomSheetRef.dismiss()
  }

  openGmaps() {
    window.open(`https://google.com/maps?q=${this.position.latitude},${this.position.longitude}`, '_self')
    this._bottomSheetRef.dismiss()
  }
}

import { Component } from '@angular/core';
import { Option } from './models/option';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent {
  options: Option[] = [
    { name: 'Visitas', icon: 'calendar_month', router: 'visitas' },
    { name: 'Médicos', icon: 'person', router: 'medicos' },
    { name: 'Clínicas', icon: 'business', router: 'clinicas' }
  ]
}

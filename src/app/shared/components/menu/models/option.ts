export interface Option {
    name: string,
    router?: string,
    icon?: string
}
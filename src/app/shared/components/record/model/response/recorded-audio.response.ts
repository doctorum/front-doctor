export default interface RecordedAudioResponse {
    blob: Blob;
    title: string;
}
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { RecordService } from './record.service';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import RecordedAudioResponse from './model/response/recorded-audio.response';

@Component({
  selector: 'app-record',
  templateUrl: './record.component.html',
  styleUrls: ['./record.component.scss']
})
export class RecordComponent implements OnInit, OnDestroy {
  @Output() newRecord = new EventEmitter<RecordedAudioResponse>();
  @Output() clearRecorded = new EventEmitter<void>(); 

  color = 'default'
  isRecord = false
  isPause = false
  blobUrl?: SafeUrl | null
  time?: string

  @Input()
  set record(record: string | undefined) {
    if(record) {
      this.blobUrl = `data:audio/wav;base64,${record}`
    }
  }

  constructor(
    private service: RecordService,
    private sanitizer: DomSanitizer
  ) {
    service.recordingTime.subscribe(time => this.time = time)
    service.recorded.subscribe(data => {
      this.blobUrl = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(data.blob))
      this.newRecord.emit(data)
    })
  }
  ngOnInit(): void {
    console.log(this.record)
  }

  ngOnDestroy(): void {
    this.saveRecord()
  }

  startRecord() {
    if(!this.isRecord && !this.isPause) {
      this.blobUrl = null
      this.isRecord = true
      this.service.startRecord()
    } else {
      this.resumeRecord()
    }
  }

  saveRecord() {
    if(this.isRecord || this.isPause) {
      this.service.saveRecord()
      this.isRecord = false
      this.isPause = false
      this.color = 'default'
    }
  }

  pauseRecord() {
    if(this.isRecord) {
      this.service.pauseRecord()
      this.isRecord = false
      this.isPause = true
      this.color = 'warn'
    }
  }

  private resumeRecord() {
    if(!this.isRecord && this.isPause) {
      this.isRecord = true
      this.isPause = false
      this.color = 'default'
      this.service.resumeRecord()
    }
  }

  clearRecord() {
    console.log('Cleaning record')
    this.blobUrl = null
    this.clearRecorded.emit()
  }
}

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/internal/Subject';

import { StereoAudioRecorder } from 'recordrtc';
import { Subscription, interval, map } from 'rxjs';
import RecordedAudioResponse from './model/response/recorded-audio.response';

@Injectable({
  providedIn: 'root'
})
export class RecordService {
  private _seconds = 0
  private stream!: MediaStream
  private recorder!: StereoAudioRecorder | null

  private _recorded = new Subject<RecordedAudioResponse>();
  private _recordingTime = new Subject<string>();
  private _recording!: Subscription

  startRecord() {
    if (this.recorder) {
      // It means recording is already started or it is already recording something
      return;
    }
    this._recordingTime.next("00:00");
    navigator.mediaDevices
      .getUserMedia({ audio: true })
      .then(stream => {
        this.stream = stream
        this.record()
      })
  }

  private record() {
    this.recorder = new StereoAudioRecorder(this.stream, {
      type: 'audio',
      mimeType: 'audio/webm'
    })

    this.recorder.record()
    this._recording = interval(1000)
      .pipe(
        map(_ => ++this._seconds),
        map(this.calculateTime)
      )
      .subscribe(timer => this._recordingTime.next(timer))
  }

  private calculateTime(seconds: number) {
    const minutes = Math.floor((seconds) / 60) % 60

    seconds = (seconds) % 60

    const minutesTime = String(minutes).padStart(2, '0')
    const secondsTime = String(seconds).padStart(2, '0')
    return `${minutesTime}:${secondsTime}`
  }

  saveRecord() {
    if (this.recorder) {
      this.recorder.stop(blob => {
        const title = `audio-${new Date().getTime()}.mp3`
        this.recorded.next({ title, blob })
      })
      this.recorder = null;
      this._seconds = 0;
      this._recording.unsubscribe()
    }
  }

  pauseRecord() {
    if(this.recorder) {
      console.log('Pause record...')
      this.recorder.pause()

      const title = `audio-${new Date().getTime()}.mp3`
      // this._recorded.next({title, blob: this.recorder.blob})
      this._recording.unsubscribe()
    }
  }

  resumeRecord() {
    if(this.recorder) {
      console.log('Resuming record...')
      this.recorder.resume()
      this._recording = interval(1000)
        .pipe(
          map(_ => ++this._seconds),
          map(this.calculateTime)
        )
        .subscribe(timer => this._recordingTime.next(timer))
    }
  }

  get recordingTime() {
    return this._recordingTime.asObservable()
  }

  get recorded() {
    return this._recorded
  }
}

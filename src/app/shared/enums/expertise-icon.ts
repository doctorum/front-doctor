export class ExpertiseIcon {
    static getIcon(expertise: string): string {
        switch (expertise) {
            case "Cardiologista": return "assets/images/cardiologista.png"
            case "Clínico Geral": return "assets/images/clinico_geral.png"
            case "Dermatologista": return "assets/images/dermatologista.png"
            case "Enfermeiro": return "assets/images/enfermeiro.png"
            case "Gastro": return "assets/images/gastro.png"
            case "Ginecologia": return "assets/images/ginecologia.png"
            case "Nutricionista": return "assets/images/nutricionista.png"
            case "Ortopedista": return "assets/images/ortopedista.png"
            case "Pediatra": return "assets/images/pediatria.png"
            default: return ""
        }
    }
}

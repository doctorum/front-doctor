export default {
    /**
     * Format Date to `yyyy-MM-dd`
     * 
     * @param date Date
     * @returns string
     *
     * @example
     * const date = new Date('2023-12-12T18:38:13.926Z')
     *
     * const result = formatDate(date)
     * console.log(result)
     * // Logs: '2023-12-12
     */
    formateDate(date: Date): string {
        date.setHours(0, 0, 0, 0)
        return date.toISOString().substring(0, 10)
    },

    /**
     * Parse string `yyyy-MM-dd` to Date
     * 
     * @param date Date on format yyyy-MM-dd
     * @returns Date
     *
     * @example
     * const date = '2023-12-12'
     *
     * const result = parseDate(date)
     * console.log(result)
     * // Logs: 'Tue Dec 12 2023 00:00:00 GMT-0300 (Brasilia Standard Time)'
     */
    parseDate(date: String): Date {
        return new Date(`${date}T00:00:00`)
    }
}
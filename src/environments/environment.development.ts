export const environment = {
    production: false,
    apiServer: 'http://localhost:4200/api',
    googleKey: '${GOOGLE_KEY}',
    clientId: '$CLIENT_ID',
    realm: '$REALM',
    url: '$SECURITY_SERVER'
};

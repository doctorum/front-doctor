export const environment = {
    production: true,
    apiServer: '$DOCTORUM_SERVER',
    googleKey: '$GOOGLE_KEY',
    clientId: '$CLIENT_ID',
    realm: '$REALM',
    url: '$SECURITY_SERVER'
};